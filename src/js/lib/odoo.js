import {createClient} from "xmlrpc"


export function endpoint(url, cookies=false) {
    return createClient({url, cookies})
}

export function auth(endpoint,db, user, password, context={})
{
    return new Promise( (resolve, error)=>{
        endpoint.methodCall('authenticate',
        [
            db, 
            user, 
            password, 
            context,
        ], function(err, result){
            if (!err)
            {
                return resolve(result)
            }
            else {
                return error(err)
            }
        })
    })
}

export function make_method(endpoint, db, uid, password, model, method){
    return function(params=[], context={}){
        return new Promise((resolve, error)=>{
            endpoint.methodCall('execute_kw',
            [
                db, 
                uid, 
                password, 
                model, 
                method, 
                params, 
                context
            ], function(err, result){
                if (!err)
                {
                    return resolve(result)
                }
                else {
                    return error(err)
                }
            })
        })
    }
}