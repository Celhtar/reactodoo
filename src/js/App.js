import { useEffect, useState } from "react"
import {endpoint, auth, make_method} from "./lib/odoo"

let common = endpoint('http://localhost/xmlrpc/2/common')
let odoo = endpoint('http://localhost/xmlrpc/2/object')
let db='preprod'
let user='admin'
let password='a'


async function get_api(common, object, db, user, password){
    let uid = await auth(common, db, user, password)
    return {
        get_partners: make_method(object, db, uid, password, 'res.partner', 'search_read'),
        get_products: make_method(object, db, uid, password, 'product.product', 'search_read')
    }
}

export function App() {
    const [products, setProducts] = useState([])
    const [partners, setPartners] = useState([])

    useEffect(async ()=> {
        let api_odoo = await get_api(common, odoo, db, user, password)
        let products = await api_odoo.get_products([], {limit: 5})
        let partners = await api_odoo.get_partners([], {limit: 30})

        setProducts(products)
        setPartners(partners)
    })
    const list_prods = products.map((product) => <li key={product.id}>{product.name}</li>)
    const list_partners = partners.map((partner) => <li key={partner.id}>{partner.name}</li>)
    return <div>
        <h1>Data from odoo</h1>
        <ul>{list_prods}</ul>
        <ul>{list_partners}</ul>
        </div>    
    ;
  }